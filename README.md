### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici donc l'énoncé, pour rappel, qui a fait l'objet de la présente solution. Il s'agit du 4ème et dernier exercice noté de la première session de ce projet

---

# Mise en application d'un pattern de couche logicielle: le nfa035.DAO

## Contexte
* Au programme de ce cours: Couches logicielles et patterns d'architecture logicielle
## Objectif
* Mise en application: Développement d'un nouveau composant logiciel modulaire mettant en application le pattern nfa035.DAO

## Consignes

Vous devez créer un nouveau module projet à l'aide d'un outil de gestion de packets propre à java: Maven
Ce module devra définir les librairies nécessaires, à l'instar du mode précédent, dans son fichier pom.xml
Ce module devra contenir la couche nfa035.DAO qui exploitera (sans y toucher pour le moment, ni le déplacer) le code produit dans l'exercice précédent, afin de sérialiser ou désérialiser un badge.
Cette couche nfa035.DAO aura pour source de donnée un "wallet" sous la forme d'un fichier csv.

Pour se faire, 

### Création d'un module Maven

 - [ ] Dans un sous-répertoire du projet nommé "**badges-wallet**", créer comme il se doit le module/projet maven qui correspondrait au code que nous avons produit jusqu'ici. Cela implique:
    - [ ] la production d'un fichier pom.xml
    - [ ] la création de l'arborescence de répertoire nécessaire aux sources et ressources de l'application ainsi que des tests
    - [ ] l'ajout des dépendances nécessaires au fichier pom.xml
    - [ ] un premier build à blanc en succès => en apporter la preuve avec un screenshot
    - [ ] ensuite, 
       - [ ] s'assurer que l'éditeur (Eclispe ou autre choix) prend bien en compte le module maven ainsi que ses dépendances
       - [ ] ajouter une dépendance manuellement vers les sources précédentes, et qui ne seront portées sous maven que plus tard...
   
    
    
### Implémentation d'un nfa035.DAO

Prendre connaissance de ce test unitaire en java:

1/ Initialisation

```java
private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

@BeforeEach
public void init() throws IOException {
    if (walletDatabase.exists()){
        walletDatabase.delete();
        walletDatabase.createNewFile();
    }
}
```

2/ Test d'écriture d'un badge

```java
 @Test
    public void testAddBadge(){
        try {

            File image = new File(RESOURCES_PATH + "petite_image.png");
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            // astuce pour ignorer les différences de formatage entre outils de sérialisation base64:
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
```

Puis:

 - [ ] Développement de la classe nfa035.DAO nécessaire à la compilation de ce test. Inutile de rappeler qu'il ne faut pas modifier ce test. Le prototype de la méthode à implémenter ici est:
```java
public void addBadge(File image) throws IOException
```

Prendre ensuite connaissance de ce test (même classe de test unitaire que le précédent):

```java
@Test
    public void testGetBadge(){
        try {
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
            dao.getBadge(memoryBadgeStream);
            byte[] deserializedImage = memoryBadgeStream.toByteArray();

            // Vérification 1
            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

            // Vérification 2
            dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet_full.csv");
            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
            dao.getBadge(fileBadgeStream);

            deserializedImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_extraite.png").toPath());
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

```

Puis:

- [ ] Développement de la classe nfa035.DAO nécessaire à la compilation de ce test. Inutile de rappeler qu'il ne faut pas modifier ce test. Le prototype de la méthode à implémenter ici est:
```java
public void getBadge(OutputStream imageStream) throws IOException
```

Pour trouver le contenu du fichier **wallet_full.csv** , il faut bien deviner qu'il s'agit de la sortie console du premier exercice indiquant le résultat de la sérialisation de la 1ère image.
Autrement dit, ceci:

```
iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQBAMAAABykSv/AAAAD1BMVEUAAABViAB+pD6WtWKrxIIz
9D4AAAAB2UlEQVR42u3csRHDMAwDQK2QFbyCV/D+M2UBFLijwzSPWqL0bC3zfELukLTuCnlC0rp0
RlrXnntAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAWIJPN6TIJkuqldRMcCAgICAgICAgI
CAgICAgICAgICAgICAgICAgICAjIBqTdfMpcZdp6TxkQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQ
EBAQEJANSPsj/QSSmtXWu8uAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIBsQNqDNy7YNjU+
GAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAVmAtMPu49D5l9MOxQcBAQEBAQEBAQEBAQEB
AQEBAQEBAQEBAQEBAQEBAfkXpH0IEDe/nLaBcXAACAgICAgICAgICAgICAgICAgICAgICAgICAgI
CAjIAqT9Gb79CX+yt710bCoICAgICAgICAgICAgICAgICAgICAgICAgICAgICMgCZDKIPh08qdde
OjYGBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBARkAdIWnHzMbx8RtJeODwZAQEBAQEBAQEBA
QEBAQEBAQEBAQEBAQEBAQEBAQEB+D/kCoPbCN0pAA6kAAAAASUVORK5C
```

 - [ ] Enfin apportez la preuve que votre test Unitaire passe bien en vert, et ce pour les 2 méthodes testées, en transmettant une photo d'écran du résultat. Attention, la façon d'intégrer son module Maven dans l'environnement de développement (Eclispe ou autre choix) est la clé pour parvenir à exécuter le test unitaire, car à ce stade, un build Maven ne pourrait pas fonctionner (dépendance sur les anciennes sources qui ne sont pas encore intégrées à Maven). Il n'y a donc que votre Eclipse (ou autre choix) pour vous permettre d'exécuter le test unitaire.


----

### Ressources ###

Vous voudrez probablement vous appuyer sur cette nouvelle implémentation de [Media](src/fr/cnam/foad/nfa035/fileutils/streaming/media/ImageFileFrame.java) disponible ;) 



